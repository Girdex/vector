

#include <iostream>
class Vector
{

public:
	
	Vector(): x(1), y(2), z(3)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		float a;
		a = sqrt( x * x + y * y + z * z);
		std::cout << x << ' ' << y << ' ' << z << " vector length = " << a;
	}

private:
	double x;
	double y;
	double z;

};


int main()
{
	Vector v;
	v.Show();
}

